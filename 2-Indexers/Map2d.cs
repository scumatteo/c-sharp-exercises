﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue> 
    {
        private Dictionary<Tuple<TKey1, TKey2>, TValue> dictionary = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other != null)
            {
                return this.Equals(other);
            }
            else
            {
                return false;
            }
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get { return this[key1, key2]; }
            set { this[key1, key2] = value; }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            List<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            list.Add(dictionary.ElementAt<>)

        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            throw new NotImplementedException();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
