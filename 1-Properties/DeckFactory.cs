﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class DeckFactory
    {

        // TODO improve
        public IList<string> Seeds
        {
            get;
            set;
        }

        // TODO improve
        public IList<string> Names
        {
            get;
            set;
        }

        public int GetDeckSize()
        {
            return Names.Count * Seeds.Count;
        }

        public ISet<Card> GetDeck()
        {
            if (this.Names == null || this.Seeds == null)
            {
                throw new InvalidOperationException();
            }
            return new HashSet<Card>(Enumerable.Range(0, Names.Count)
                .SelectMany(i => Enumerable.Repeat(i, Seeds.Count)
                    .Zip(Enumerable.Range(0, Seeds.Count), (n, s) => Tuple.Create(Names[n], Seeds[s], n))
                ).Select(tuple => new Card(tuple))
                .ToList());
        }
    }

}
