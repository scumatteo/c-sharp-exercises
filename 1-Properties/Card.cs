﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string Seed
        {
            get { return this.seed; }
        }

        // TODO improve
        public string Name
        {
            get { return this.name; }
        }

        // TODO improve
        public int Ordial
        {
            get { return this.ordial; }
        }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordial})";
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(this, obj))
            {
                return true;
            }
            else if(obj is Card)
            {
                Card other = (Card)obj;
                return Seed.Equals(other.Seed) 
                    && Name.Equals(other.Name) 
                    && Ordial.Equals(other.Ordial);

            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            int x = 31;
            return x* this.Seed.GetHashCode()
                + x*x* this.Name.GetHashCode()
                + x*x*x* this.Ordial.GetHashCode();

        }
    }

}